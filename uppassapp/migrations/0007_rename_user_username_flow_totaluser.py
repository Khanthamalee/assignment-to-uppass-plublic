# Generated by Django 4.1.1 on 2022-10-10 15:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uppassapp', '0006_alter_contact_nameflow_addresscountryflow_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='username_flow',
            old_name='user',
            new_name='totaluser',
        ),
    ]
