from django.urls import path
from .views import *


urlpatterns = [
    path('',Home, name = 'Home-Page'),
    path('send-form-to-customer/<int:uid>/<str:token>/',Sendformtocustomer, name = 'Send-form-to-customer'),
    #path('flow-create/<int:uid>/',Addcontactflow, name = 'Flow-Page'),
    #path('login-user/',Loginuser, name = 'Loginuser-Page'),
    #path('filter-user/',Filteruser, name = 'Filteruser-Page'),
    path('profileuser/',Profileuser, name = 'Profileuser-Page'),

    path('register/',Register, name= 'Register-page'),

    path('reset-password/',ResetPassword, name= 'reset-password'),
    path('reset-new-password/<str:token>/',ResetNewPassword, name= 'Reset-New-Password-page'),

    path('verify_success/<str:token>/',Verify_success, name= 'Verify_success-page'),

    path('Flow-Build/',FlowBuild, name= 'FlowBuild-page'),
    #path('Flow-group/',Flowgroup, name= 'Flowgroup-page'),
    path('Myfrom-user/',Myfrom, name= 'Myfrom-page'),
    
    
    #path('create-flow/',CreateFlow, name= 'CreateFlow-page'),
    path('detail-cutomer/<int:did>/',Detailcutomer, name= 'Detailcutomer-page'),
    #path('detail-Groupe-from-id/',DetailGroupefromid, name= 'Detail-Groupe-from-id-page'),

    #ทำหน้า createflow
    path('crud-page/',CrudView.as_view(), name= 'Crud-ajax'),
    path('ajax/crud-page/create/',CreateCrudUser.as_view(), name='crud_ajax_create'),
    path('ajax/crud-page/update/',UpdateCrudUser.as_view(), name='crud_ajax_update'),
    path('ajax/crud-page/delete/',DeleteCrudUser.as_view(), name='crud_ajax_delete'),

    #ทำหน้า Groupefromid
    #path('recieve-crud-view/',RecieveCrudView.as_view(), name= 'RecieveCrudView'),
    #path('ajax/crud-page/create/',CreateCrudUser.as_view(), name='crud_ajax_create'),
    #path('ajax/crud-page/update/',UpdateCrudUser.as_view(), name='crud_ajax_update'),
    #path('ajax/crud-page/delete/',DeleteCrudUser.as_view(), name='crud_ajax_delete'),
]