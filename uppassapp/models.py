from django.db import models
from django.contrib.auth.models import User

class AllElement(models.Model):
    iconelement = models.ImageField(null=True,blank=True)
    nameelement = models.CharField(max_length=200)
    def __str__(self):
        return self.nameelement

class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	usertype = models.CharField(max_length = 100, default= 'member')
	point = models.IntegerField(default=0)
	mobile = models.CharField(max_length = 20,null=True,blank=True)
	verified = models.BooleanField(default=False)
	verify_token = models.CharField(max_length = 100,default='no  token')

	def __str__(self):
		return self.user.username

class Username_flow(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE,null=True,blank=True)
	nameflow = models.CharField(max_length=200)
	token = models.CharField(max_length=100,null=True,blank=True)
	def __str__(self):
		return '{}-{}'.format(self.user.username,self.nameflow)


class Contact_nameflow(models.Model):
	allflow = models.ForeignKey(Username_flow,on_delete=models.CASCADE,null=True,blank=True)
	name_form = models.CharField(max_length=200,null=True,blank=True)
	prefixflow = models.CharField(max_length=200)
	firstnameflow = models.CharField(max_length=200)
	midlenameflow = models.CharField(max_length=200,null=True,blank=True)
	lastnameflow = models.CharField(max_length=200)
	genderflow = models.CharField(max_length=200,null=True,blank=True)
	phoneflow = models.CharField(max_length=11,null=True,blank=True)
	emailflow = models.CharField(max_length=200,null=True,blank=True)
	addresscountryflow = models.CharField(max_length=200)
	addressflow = models.TextField(default=True)
	addressprovinceflow = models.CharField(max_length=200)
	addressdistrictflow = models.CharField(max_length=200)
	addresssubdistrictflow = models.CharField(max_length=200)
	pictureflow = models.ImageField(upload_to ='product',null=True,blank=True)
	specfileflow = models.FileField(upload_to ='specfile',null=True,blank=True)
	
	def __str__(self):
		return '{}-{}-{}'.format(self.allflow.nameflow,self.firstnameflow,self.lastnameflow) 

class ResetPasswordToken(models.Model):
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	token = models.CharField(max_length=100)
	
	def __str__(self):
		return self.user.username



'''class Contact_genderflow(models.Model):
	contactnameflow = models.ForeignKey(Contact_nameflow,on_delete=models.CASCADE)
	genderflow = models.CharField(max_length=200,null=True,blank=True)
	phoneflow = models.CharField(max_length=11)
	emailflow = models.CharField(max_length=200)
	def __str__(self):
		return '{}-{}'.format(self.contactnameflow,self.genderflow) 	

class Contact_addressflow(models.Model):	
	contactgenderflow = models.ForeignKey(Contact_genderflow,on_delete=models.CASCADE)
	addresscountryflow = models.TextField(default=True)
	addressflow = models.TextField(default=True)
	addressprovinceflow = models.TextField(default=True)
	addressdistrictflow = models.TextField(default=True)
	addresssubdistrictflow = models.TextField(default=True)
	pictureflow = models.ImageField(upload_to ='product',null=True,blank=True)
	specfileflow = models.FileField(upload_to ='specfile',null=True,blank=True)
	def __str__(self):
		return '{}-{}'.format(self.contactgenderflow,self.addressprovinceflow)''' 

