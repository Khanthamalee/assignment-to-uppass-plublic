from django.apps import AppConfig


class UppassappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uppassapp'
