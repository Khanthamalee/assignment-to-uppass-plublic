##กรณีที่ต้องการให้โชว์เป็นพารากราฟจะใช้ HttpResponse
##from django.http import HttpResponse

#อันนี้ต้องการดึงมาเป็นหน้า html ใช้ render และ ต้องการให้ทำบางอย่างแล้วไปที่หน้าอื่นให้ใช้ redirect

from multiprocessing.sharedctypes import Value
from django.shortcuts import render,redirect

#import class ใน model มาให้หมดใช้ *
from .models import *

# ระบบ log in 
from django.contrib.auth import authenticate,login

#จะไม่สามารถเข้าชมข้างในเว็ปไซต์ได้ถ้าไม่ได้เป็นสมาชิก
from django.contrib.auth.decorators import login_required

#ส่งเมลล์เพื่อ verify website#
from .sendgmail import sendthai

#อัพโหลดไฟล์พวกรูปภาพและ pdf
from django.core.files.storage import FileSystemStorage

#ใช้สำหรับทำหน้ารับข้อมูลแล้ว return เป็น Json
from django.views.generic import  View
from django.http import JsonResponse


#ใช้ในกรณีที่ ต้อง filter model ใน class CrudView
from django.views import generic



def Home(request):
    return render(request,'uppassapp/loginuser.html')

#defCreateFlow(request):
	#if request.method =='POST':
		#data = request.POST.copy()
	#return render(request,'uppassapp/createflow.html')


def Login(request):
	context = {}
	if request.method == 'POST':
		data = request.POST.copy()
		username = data.get('username')
		password = data.get('password')
		try:
			user = authenticate(username = username, password = password)
			login(request,user)
			return redirect('Crud-ajax')
		except:
			context['message'] = 'Username or password incorrect!!,please check the correctness or reset your password.'

	return render(request,'uppassapp/loginuser.html',context)


import uuid
def ResetPassword(request):

	context = {}

	if request.method == 'POST':
		#รับข้อมูลจาก resetpassword.html
		data = request.POST.copy()
		username = data.get('username')

		try: 
			user = User.objects.get(username=username)   #ตรวจสอบว่ามี email นี้ในระบบหลังบ้านเราไหม
			u = uuid.uuid1()
			token = str(u)
			newreset = ResetPasswordToken()
			newreset.user = user
			newreset.token = token
			newreset.save()

			#ข้อความใน e-mail ของ username ที่ต้องการ reset password
			text = 'Please click this link for reset password.\n\nLink : http://localhost:8000/reset-new-password/'+token
			sendthai(username,'resetpassword link(n.Khanthamalee)',text)

			#ข้อความบน reset-password ที่จะปรากฏเมื่อเรากรอก username เรียบร้อยแล้ว
			context['message'] = 'The reset password link was send to you e.mail, please check the last your e-mail.'
			return render(request,'uppassapp/resetpassword.html',context)

		except:
			#ข้อความบน reset-password ที่จะปรากฏเมื่อเรากรอก username ที่ไม่มีในระบบ database
			context['message'] = 'Your e-mail has not the system, please check the correctness'

	return render(request,'uppassapp/resetpassword.html',context)


##uuid มาสุ่ม token ในหน้าสมัครสมาชิก
import uuid
def Register(request):
	context = {}

	if request.method == 'POST':
		#รับข้อมูลจาก Register.html
		data = request.POST.copy()
		fullname = data.get('fullname')
		mobile = data.get('mobile')
		username = data.get('username') #username คือ email นะจ๊ะ 
		password = data.get('password')
		password2 = data.get('password2')


		try: 
			check = User.objects.get(username=username)   #ตรวจสอบว่ามี email นี้ในระบบหลังบ้านเราไหม
			context['warning']='You can not use {}, Please use a new e-mail'.format(username)
			return render(request,'uppassapp/registeruser.html',context)   #ถ้าไม่มีให้ปรากฏ warning และหน้า Register.html

		except:

			#ถ้ายังไม่มี email นี้ในระบบหลังบ้านให้ >>

			# 1. ตรวจสอบ password
			if password != password2:   #ตรวจสอบ password ว่าถูกต้องไหม
				context['warning']='Please enter the same password'
				return render(request,'uppassapp/registeruser.html',context) #ถ้าไม่มีให้ปรากฏ warning และหน้า Register.html

			# 2. เพิ่มข้อมูลใน User
			newuser = User()    #User ใหม่ คือ User ในหลังบ้าน 
			newuser.username = username    #email ใหม่ใน Register.html คือ username ในหลังบ้าน 
			newuser.email = username     #email ใหม่ใน Register.html คือ email ในหลังบ้าน
			newuser.first_name = fullname   #fullname ใหม่ใน Register.html คือ first_name ในหลังบ้าน
			newuser.set_password(password)  #password ใหม่ใน Register.html คือ set_password ในหลังบ้าน 
			                                #> set_password(password ใน html)
			newuser.save()    #บันทึกข้อมูลทั้งหมด

			u = uuid.uuid1()
			token = str(u)  #สุ่ม

			# 3. เพิ่มข้อมูลใน Profile
			newprofile = Profile()    #สร้างโปรไฟล์ใหม่หลังจากมีการสมัครสามาชิก
			newprofile.user = User.objects.get(username=username)    #รับ username เป็น email จาก Register.html ใน User
			                                                         #มาเป็น user ใน profile
			newprofile.mobile = mobile    #mobile เป็น mobile ใหม่จาก Register.html จะบันทึกใน mobile class Profile
			newprofile.verify_token = token
			newprofile.save() #บันทึกข้อมูลทั้งหมด


			#ถ้าสมัครสมาชิกแล้ว จะปรากฏ text ใน email ดังนี้ 
			text = 'Please enter this link for confirmation of membership at n.Khanthamalee website\n\n Link: http://localhost:8000/verify_success/'+token
			sendthai(username,'n.Khanthamalee Website',text)
			

		try:
			user = authenticate(username = username, password = password)
			login (request,user)
			return redirect('FlowBuild-page')
		except:
			context['message'] = 'Username or Password incorrect'

	return render(request,'uppassapp/registeruser.html',context)

def ResetNewPassword(request,token):

	context = {}
	print('token:',token)

	try:
		check = ResetPasswordToken.objects.get(token=token)   #ตรวจสอบว่า token = token ใน database ไหม

		#ดึงข้อมูลจาก reset-new-password 
		if request.method == 'POST':
			data = request.POST.copy()
			password1 = data.get('resetpassword1')
			password2 = data.get('resetpassword2')

			if password1 == password2:    #ถ้า password ทั้งสองเหมือนกันให้ทำต่อไป
				user = check.user    #user เท่า user ใน model ResetPasswordToken 
				user.set_password(password1)   #set password1 เป็น password ของ user
				user.save()    #บันทึกข้อมูลง database
				user = authenticate(username=user.username,password=password1) 
				login(request,user)
				return redirect('Profileuser-Page')   #login แล้วปรากฏหน้า ProfileUser-page

			else:    #ถ้า password ทั้งสองไม่เหมือนกันให้ เตือน error บน reset-new-password
				context['error'] = 'Please enter the same password.'

	except:    #ถ้าหน้าที่เข้าไปเพืท่อ reset password ไม่ใช่ token ใน database จะปรากฏข้อความ

		#แล้วบอกให้ reset password ใหม่อีกครั้ง ซึ่ง return เข้าหน้า resetpassword ให้แล้ว
		context['error'] = 'Reset password link incorrect, please reset password again'
		return render(request,'newapp/resetpassword.html',context)  

	return render(request,'uppassapp/resetnewpassword.html',context)

def Verify_success(request,token):
	context = {}

	try:
		check = Profile.objects.get(verify_token = token)
		check.verified = True
		check.point = 100
		check.save()

	except:
		context['error'] = 'This is not confirmation link, please click link from your e-mail.'

	return render(request,'uppassapp/verifyemail.html',context)

@login_required
def FlowBuild(request):
    return render(request,'uppassapp/flowbuild.html')

	
#สร้างหน้าของโปรไฟล์ user สามารถดูข้อมูลของตัวเองได้แต่ยังไม่ทำให้แก้ไขได้
@login_required
def Profileuser(request): #ใช้ Profile ไม่ได้เพราะ class model มีชื่อนี้แล้ว
	context = {}
	profileuser = Profile.objects.get(user=request.user)
	context['profile'] = profileuser
	return render(request,'uppassapp/Profileuser.html',context)
#ทด'''
'''@login_required
def Flowgroup(request): #หน้ารวมของแบบ from ทั้งหมด
	username = request.user.username
	print("username",username)
	user = User.objects.get(username=username)
	print("user",user)
	formgroup = Contact_nameflow.objects.all()
	context = {'formgroup': formgroup}
	for i in formgroup:
		print("formgroup :", i)
	myform = Username_flow.objects.all()
	for i in myform:
		print("myform :",i )
	myformget = Username_flow.objects.filter(user=user)
	for i in myformget:
		print("myformget :",i )
	formgroupandmyform = Contact_nameflow.objects.filter(allflow__user =user)
	for i in formgroupandmyform:
		print("formgroupandmyform :",i)

	return render(request,'uppassapp/flowgroup.html',context)'''

@login_required
def Myfrom(request):
	
	username = request.user.username
	#print("username",username)
	user = User.objects.get(username=username)
	#print("user",user)
	myform = Username_flow.objects.filter(user=user)
	#context = {'myform': myform}
	#for i in myform:
		#print("myform: ", i )

	formgroupandmyform = Contact_nameflow.objects.filter(allflow__user = user)
	listinfrom = formgroupandmyform
	context = {'myform': myform, 'listinfrom': listinfrom}

	#for i in listinfrom:
		#print("listinfrom :",i.prefixflow)
		# print("listinfrom :",i.firstnameflow)

	#formgroup = Contact_nameflow.objects.get(allflow=myform)
	#context ['formgroup']: formgroup
	#print(context)
	print("context",context)

	return render(request,'uppassapp/flowgroup.html',context)
#addcontactflow to database



# ข้อมูลจาก https://studygyaan.com/django/how-to-execute-crud-using-django-ajax-and-json?fbclid=IwAR3C9izch2rApw7JuIFgm36SrZlC3fX2w4L8SU4arzlXwicxVI7_dqkF5m0


# class CrudView(ListView):
# 	model = Username_flow
# 	template_name = 'uppassapp/crud.html'
# 	context_object_name = 'users'


# def CrudView(request):
# 	user = Username_flow.objects.all()
# 	context = {'users':user}
# 	return render(request,'uppassapp/createflow.html',context)


#import ด้านบน
#from django.views.generic import  View
#from django.http import JsonResponse


#ใช้ในกรณีที่ ต้อง filter model ใน class CrudView
from django.views import generic
#ดึงข้อมูลจากโมเดลและ createflow.html มา และส่งกลับ users ไปที่ html
class CrudView(generic.ListView):
	Model = Username_flow
	template_name = 'uppassapp/createflow.html'
	context_object_name = 'Myform'
	

	def get(self, request):
		userlogin = User.objects.get(username=request.user.username)
		myform = Username_flow.objects.filter(user=userlogin)
		Myform = myform
		#print(Myform)
		return render(request,self.template_name,{'Myform': Myform})

	

#รับข้อมูลจาก crud.html แล้วเอาไปเขียนในฐานข้อมูลและส่งไปที่ตารางโดนใช้ Javascript
class CreateCrudUser(View):

	def get(self, request):
		name2 = request.GET.get('name1', None)
		address2 = request.GET.get('address1', None)
		#token = request.GET.get('token', None)
		print(name2)
		print(address2)
		
		
		
		obj = Username_flow.objects.create(
			user = User.objects.get(username=name2),
			nameflow = address2,
			token = str(uuid.uuid1())
		)
		
		
		#user = Username_flow.objects.filter(Q(nameflow__icontains=name2))
		#context = {'user':user}
		#print('context : ', context)

		print(Username_flow.objects.all())

		
		#usernamelogin = request.user.username
		#print("usernamelogin",usernamelogin)
		#userlogin = User.objects.get(username=usernamelogin)
		#print("userlogin",userlogin)
		#myform = Username_flow.objects.filter(user=userlogin)
		#print(data)
		#
			#print("myform",i)
		
		usernamelogin = request.user.username
		#print("usernamelogin",usernamelogin)
		userlogin = User.objects.get(username=usernamelogin)
		#print("userlogin",userlogin)
		myform = Username_flow.objects.filter(user=userlogin)


		for form in myform:
			#print("form.nameflow",form.nameflow)
			user = {'id':form.id,
			'name1': form.user.username,
			'address1': form.nameflow,
			'token': 'http://localhost:8000/send-form-to-customer/'+ str(form.token),
			}
			
		print('user : ',user)

		data = {'user':user}
		#print('data :',data)
		
		return JsonResponse(data)


#อัพเดทเดต้า ป๊อปอัพในการอัพเดท ที่เราคลิก Edit#
import uuid
class UpdateCrudUser(View):
	def  get(self, request):
		id1 = request.GET.get('id', None)
		name1 = request.GET.get('name', None)
		address1 = request.GET.get('address', None)
		age1 = request.GET.get('age', None)
		
		print(id1)
		print(name1)
		print(address1)
		print(age1[80:])
		obj = Username_flow.objects.get(id=id1)
		obj.user = User.objects.get(username=name1)
		obj.nameflow  = address1
		obj.save()
		
		
		user = {
			'id':obj.id,
			'name':name1,
			'address':obj.nameflow,
			'age':age1
		}
			
		data = {
            'user': user
        }
		print(data)
		return JsonResponse(data)

#ลบข้อมูล  ป๊อปอัพจะโชวเมื่อเราคลิก Delete #

class DeleteCrudUser(View):
    def  get(self, request):
        id1 = request.GET.get('id', None)
        Username_flow.objects.get(id=id1).delete()
        data = {
            'deleted': True
        }
        return JsonResponse(data)  

def Sendformtocustomer(request,token,uid):

	context = {}
	try:
		check = Username_flow.objects.get(token=token)   #ตรวจสอบว่า token = token ใน database ไหม

	except:    #ถ้าหน้าที่เข้าไปเพืท่อ reset password ไม่ใช่ token ใน database จะปรากฏข้อความ

		#แล้วบอกให้ reset password ใหม่อีกครั้ง ซึ่ง return เข้าหน้า resetpassword ให้แล้ว
		context['error'] = 'แบบฟอร์มนี้ได้ถูกยกเลิกไปแล้ว กรุณาติดต่อแอดมินค่ะ'

	usernameflow = Username_flow.objects.get(id=uid)
	context['usernameflow']=usernameflow 

	if request.method == 'POST':
		context = {}

		data = request.POST.copy()

		#Prefix
		allprefix = data.get('radioprefix')
		
		#Fullname
		firstname = data.get('firstname')
		middlename = data.get('middlename')
		lastname = data.get('lastname')

		#Gender
		genders = data.get('radiogender')
		
		#Phone
		phone = data.get('phone')

		#Email
		email = data.get('email')
		emailconfirm = data.get('emailconfirm')

		#Address
		address1 = data.get('address1')
		address2 = data.get('address2')
		address3 = data.get('address3')
		address4 = data.get('address4')
		address5 = data.get('address5')
		address6 = data.get('address6')


		print(allprefix)
		print(firstname)
		print(middlename)
		print(middlename)
		print(lastname)
		print(genders)
		print(phone)
		print(email)
		print(emailconfirm)
		print(address1)
		print(address2)
		print(address3)
		print(address4)
		print(address5)
		print(address6)
		print('File:',request.FILES)


		
		
		flowcontact = Contact_nameflow()
		flowcontact.allflow  = usernameflow 
		

		# Usernameflowmodel = Username_flow()
		
		
		# Usernameflowmodel.totaluser = User.objects.get(username=username3)

		# flowcontact.userandnameflow = Username_flow.objects.get(totaluser=Usernameflowmodel.totaluser)
		# flowcontact.allflow  = Username_flow.objects.get(
		# 	nameflow=Username_flow.objects.get(username=namecreate),
		# 	totaluser = Username_flow.objects.get(username=namecreate)
		# 	)

		flowcontact.name_form = usernameflow.nameflow
		flowcontact.prefixflow = allprefix
		flowcontact.firstnameflow = firstname
		flowcontact.midlenameflow = middlename
		flowcontact.lastnameflow = lastname
		flowcontact.genderflow = genders
		flowcontact.phoneflow = phone

		if email == emailconfirm:
			flowcontact.emailflow = email

		flowcontact.addresscountryflow = address1
		flowcontact.addressflow = address2
		flowcontact.addressprovinceflow = address3
		flowcontact.addressdistrictflow = address4
		flowcontact.addresssubdistrictflow = address5

		
		if 'picture' in request.FILES:
			file_image = request.FILES['picture']
			file_image_name = file_image.name.replace(' ','')
			print(file_image_name)
			fs = FileSystemStorage(location='media/product')
			filename = fs.save(file_image_name,file_image)
			upload_file_url = fs.url(filename)
			flowcontact.pictureflow = '/product' + upload_file_url[6:]
			print('upload_file_url[6:]:',upload_file_url[6:])
		
		if 'specfile' in request.FILES:
			file_image = request.FILES['specfile']
			file_image_name = file_image.name.replace(' ','')
			print(file_image_name)
			fs = FileSystemStorage(location='media/specfile')
			filename = fs.save(file_image_name,file_image)
			upload_file_url = fs.url(filename)
			flowcontact.specfileflow = '/specfile' + upload_file_url[6:]
			print('upload_file_url[6:]:',upload_file_url[6:])

		context['usernameflow']= flowcontact
		flowcontact.save()
		context['message'] = 'บันทึกข้อมูลเสร็จเรียบร้อยแล้ว'

	return render(request,'uppassapp/flow.html',context)



@login_required
def Detailcutomer(request,did): 
	context = {}
	contactnameflow = Contact_nameflow.objects.get(id=did)
	context['contactnameflow'] = contactnameflow

	if request.method =='POST':
		data = request.POST.copy()
	
		if 'delete' in data:
			try:
				contactnameflow.delete()
				return redirect('Flowgroup-page')
			except:
				pass

	return render(request,'uppassapp/flowdata.html',context)
