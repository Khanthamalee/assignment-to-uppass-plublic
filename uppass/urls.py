"""uppass URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
#include urls fron app
from django.urls import path,include
from uppassapp.views import Login
from django.contrib.staticfiles.urls import static,staticfiles_urlpatterns

#ในกรณีที่ต้องการอัพโหลดภาพหรือไฟล์ลง database จะต้องมี
from . import settings

#import data from views
from django.contrib.auth import views

urlpatterns = [
    path('admin/', admin.site.urls),
    #สร้างตัวเชื่อมระหว่าง urls ของแอพกับ urls ของ website
    path('', include('uppassapp.urls')),

    #path('Home/',Home, name = 'Home-Page'),
    #path('Home/Fullname/',Fullname, name = 'Fullname-Page'),
    #path('login-user/',Loginuser, name = 'Loginuser-Page'),
    #path('full-name/',Fullname, name = 'Fullname-Page'),
    #path('profileuser/',Profileuser, name = 'Profileuser-Page'),
    path('login/',Login,name = 'login'),
    path('logout/',views.LogoutView.as_view(template_name = 'uppassapp/logout.html'),name = 'logout'),
    #path('register/',Register, name= 'Register-page'),
]


#ในกรณีที่ต้องการอัพโหลดภาพหรือไฟล์ลง database จะต้องมี
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
